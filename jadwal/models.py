from django.db import models

# Create your models here.
class Jadwal(models.Model) :
    hari = models.CharField(max_length=10, blank=False)
    tanggal = models.DateField(blank=False)
    jam = models.TimeField(blank=False)
    namaKegiatan = models.TextField(max_length=100, blank=False)
    tempat = models.TextField(max_length=50, blank=False)
    kategori = models.CharField(max_length=20, blank=False)
