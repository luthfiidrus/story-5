from django import forms

class formulir(forms.Form) :
    day = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Hari',
        'type' : 'text',
        'required' : True
    }))

    date = forms.DateField(widget=forms.DateInput(attrs={
        'class' : 'form-control',
        'placeholder' : "Tanggal",
        'type' : 'date',
        'required' : True
    }))

    time = forms.TimeField(widget=forms.TimeInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Pukul',
        'type' : 'time',
        'required' : True
    }))

    activity = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Nama Kegiatan',
        'type' : 'text',
        'required' : True
    }))

    place = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Tempat Kegiatan',
        'type' : 'text',
        'required' : True
    }))

    category = forms.CharField(widget=forms.TextInput(attrs={
        'class' : 'form-control',
        'placeholder' : 'Kategori Kegiatan',
        'type' : 'text',
        'required' : True
    }))
