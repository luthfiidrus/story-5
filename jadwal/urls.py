from django.urls import path
from . import views

urlpatterns = [
    path('', views.sched),
    path('<int:pk>', views.delete, name="delete")
]
