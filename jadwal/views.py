from django.shortcuts import render, redirect
from .forms import formulir
from .models import Jadwal

# Create your views here.
def sched(request) :
    if request.method == 'POST':
        form = formulir(request.POST)
        if (form.is_valid()) :
            jadwalBaru = Jadwal()
            jadwalBaru.hari = form.cleaned_data['day']
            jadwalBaru.tanggal = form.cleaned_data['date']
            jadwalBaru.jam = form.cleaned_data['time']
            jadwalBaru.namaKegiatan = form.cleaned_data['activity']
            jadwalBaru.tempat = form.cleaned_data['place']
            jadwalBaru.kategori = form.cleaned_data['category']
            jadwalBaru.save()
        return redirect('/jadwal')
    else :
        form = formulir()
        schedule = Jadwal.objects.all()
        context = {
            'formulir' : form,
            'schedule' : schedule
        }
        return render(request, 'jadwal/jadwal.html', context)

def delete(request, pk) :
    if request.method == 'POST':
        form = formulir(request.POST)
        if (form.is_valid()) :
            jadwalBaru = Jadwal()
            jadwalBaru.hari = form.cleaned_data['day']
            jadwalBaru.tanggal = form.cleaned_data['date']
            jadwalBaru.jam = form.cleaned_data['time']
            jadwalBaru.namaKegiatan = form.cleaned_data['activity']
            jadwalBaru.tempat = form.cleaned_data['place']
            jadwalBaru.kategori = form.cleaned_data['category']
            jadwalBaru.save()
        return redirect('/jadwal')
    else :
        Jadwal.objects.filter(pk=pk).delete()
        form = formulir()
        schedule = Jadwal.objects.all()
        context = {
            'formulir' : form,
            'schedule' : schedule
        }
        return render(request, 'jadwal/jadwal.html', context)
